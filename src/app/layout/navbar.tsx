import React from "react";
import Image from "next/image";
import thaiIcons from "@/assets/images/icons/thai.svg";
import userMock from "@/assets/images/avatar/user.png";
import logo from "@/assets/images/icons/logo.svg";
import search from "@/assets/images/icons/search.svg";
import Dropdown from "../components/dropdown";

type Props = {};

export default function Navbar({}: Props) {
  return (
    <nav>
      <div className="flex h-20 px-8 py-0 justify-between items-center w-full">
        <div className="flex items-center gap-12 left-0" id="left-content">
          <div
            className="flex p-2 flex-col justify-center items-center gap-2 self-stretch"
            id="logo"
          >
            <Image
              src={logo}
              width={137}
              height={48}
              alt=""
              className="h-12"
            ></Image>
          </div>
          <div id="searchbox" className="flex gap-4">
            <Dropdown />
            <Dropdown />
          </div>
        </div>
        <div className="flex items-center gap-8 right-0" id="right-content">
          <div className="flex px-2 py-3 items-center">
            <div className="flex items-center gap-0.5">
              <Image src={thaiIcons} width={32} height={32} alt=""></Image>
              <p>TH</p>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="32"
                viewBox="0 0 24 32"
                fill="none"
              >
                <path
                  d="M16 12C16.412 12 16.6472 12.4703 16.4 12.8L12.4 18.1333C12.2 18.4 11.8 18.4 11.6 18.1333L7.6 12.8C7.35279 12.4703 7.58798 12 8 12L16 12Z"
                  fill="white"
                />
              </svg>
            </div>
          </div>
          <div className="flex items-center gap-2" id="profile">
            <Image
              src={userMock}
              width={44}
              height={44}
              alt=""
              className="rounded-full w-11 h-11 shrink-0"
            ></Image>

            <div className="flex flex-col items-start" id="information">
              <div className="flex items-baseline gap-1 text-sm" id="title">
                <p className="font-semibold text-sm">กมลวรรณ</p>
              </div>
              <div className="text-sm" id="details">
                IoT Employee
              </div>
            </div>
            <button className="" id="profile-dropdown">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                fill="none"
              >
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M6.46967 9.46967C6.76256 9.17678 7.23744 9.17678 7.53033 9.46967L12 13.9393L16.4697 9.46967C16.7626 9.17678 17.2374 9.17678 17.5303 9.46967C17.8232 9.76256 17.8232 10.2374 17.5303 10.5303L12.5303 15.5303C12.2374 15.8232 11.7626 15.8232 11.4697 15.5303L6.46967 10.5303C6.17678 10.2374 6.17678 9.76256 6.46967 9.46967Z"
                  fill="white"
                />
              </svg>
            </button>
          </div>
        </div>
      </div>
    </nav>
  );
}
