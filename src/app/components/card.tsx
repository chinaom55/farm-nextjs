import React, { useEffect } from "react";
// import icons image
import Image from "next/image";
import IconLink from "@/assets/images/icons/link.svg";
import IconBoard from "@/assets/images/icons/board.svg";
import IconSensor from "@/assets/images/icons/sensor.svg";

type Props = {
  id: string;
  name: string;
  status: boolean;
  count: string;
  type: string;
};

export default function Card({ id, name, status, count, type }: Props) {
  let src;

  if (type === "board") {
    src = IconBoard;
  } else if (type === "sensor") {
    src = IconSensor;
  }

  return (
    <div id={id}>
      <div className="flex items-start pt-4 pl-6 pb-4 pr-4 card-status rounded-xl justify-between">
        <div className="flex flex-col gap-2 items-start w-[148px]">
          <div
            className={`p-1 justify-center items-center rounded-lg ${
              type === "sensor"
                ? !status
                  ? "bg-warning"
                  : "bg-success"
                : type === "board"
                ? !status
                  ? " bg-red-600"
                  : " bg-green-600"
                : ""
            }`}
          >
            <Image src={src} width={36} height={36} alt=""></Image>
          </div>
          <span>
            <p className="font-normal text-sm text-gray">{name}</p>
            <h2>{count}</h2>
          </span>
        </div>
        <div>
          <Image src={IconLink} width={24} height={24} alt=""></Image>
        </div>
      </div>
    </div>
  );
}
