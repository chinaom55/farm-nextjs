"use client";
import React from "react";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  Tooltip,
  PointElement,
  LineElement,
} from "chart.js";
import { Line } from "react-chartjs-2";

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Tooltip
);

type Props = {};

ChartJS.register(CategoryScale /* ... */);

export default function Chart({}: Props) {
  return (
    <div className="flex w-full">
      <Line
        data={{
          labels: ["ม.ค.", "ก.พ.", "ม.ค.", "เม.ษ.", "พ.ค.", "มิ.ย."],
          datasets: [
            {
              data: [1700, 1400, 2100, 1600, 1300, 2200],
              backgroundColor: "white",
            },
          ],
        }}
      />
    </div>
  );
}
