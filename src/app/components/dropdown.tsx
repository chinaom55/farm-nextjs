"use client";
import React, { useState } from "react";

type Props = {};

export default function Dropdown({}: Props) {
  const [selectedOption, setSelectedOption] = useState("");

  const handleDropdownChange = (
    event: React.ChangeEvent<HTMLSelectElement>
  ) => {
    setSelectedOption(event.target.value);
  };
  return (
    <select
      value={selectedOption}
      onChange={handleDropdownChange}
      className="flex px-4 py-2.5 gap-2 self-stretch bg-white rounded-lg items-center font-normal text-gray-400 text-base w-[224px]"
    >
      <option value="">ทั้งหมด</option>
      <option value="option1">ชื่อหน่วยงาน</option>
      <option value="option2">ชื่อโครงการ</option>
      <option value="option3">ID โครงการ</option>
      <option value="option4">ชื่อผู้ดูแล</option>
    </select>
  );
}
