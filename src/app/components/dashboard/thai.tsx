import React, { useState } from "react";
import Card from "@/app/components/card";

// import icons image
import Image from "next/image";
import IconLink from "@/assets/images/icons/link.svg";
import Dropdown from "../dropdown";
import Tab from "../tab";
import Chart from "../chart";

type Props = {
  title: string;
};

const data = {
  department: 25,
  project: "1,200",
  board: "36,000",
  sensor: "612,000",
};

const mockData = {
  sensors: [
    {
      id: "sensor-offline",
      name: "เซนเซอร์ออฟไลน์",
      status: false,
      count: "1,080",
      type: "sensor",
    },
    {
      id: "sensor-online",
      name: "เซนเซอร์ออนไลน์",
      status: true,
      count: "610,920",
      type: "sensor",
    },
  ],
  boards: [
    {
      id: "board-offline",
      name: "บอร์ดออฟไลน์",
      status: false,
      count: "216",
      type: "board",
    },
    {
      id: "board-online",
      name: "บอร์ดออนไลน์",
      status: true,
      count: "34,704",
      type: "board",
    },
  ],
};

export default function DashboardThai({ title }: Props) {
  return (
    <div className="grid col-span-4 items-start gap-16">
      <div className="flex flex-col gap-4">
        <h2>{title}</h2>
        <div className="flex gap-8">
          <div className="flex w-[304px] pt-4 pl-6 pb-4 pr-4 justify-between items-start bg-blue-600 rounded-xl">
            <div className="flex flex-col">
              <p className="font-normal text-sm text-gray">หน่วยงาน</p>
              <h2>{data.department} หน่วยงาน</h2>
            </div>
            <div>
              <Image src={IconLink} width={24} height={24} alt=""></Image>
            </div>
          </div>
          <div className="flex flex-col justify-center">
            <p className="font-normal text-sm text-gray">โครงการทั้งหมด</p>
            <p className=" font-semibold text-2xl">{data.project} โครงการ</p>
          </div>
        </div>
      </div>
      <div className="flex gap-4 items-start self-stretch">
        <div className="flex flex-col gap-4 items-start w-[440px]">
          <div id="board" className="">
            <h2>บอร์ด</h2>
            <div className="flex items-center">
              <p className="text-2xl font-bold leading-8">
                ทั้งหมด
                <span className="text-3xl font-bold ps-1.5 pe-1.5">
                  {data.board}
                </span>
                บอร์ด
              </p>
            </div>
          </div>
          <div className="flex items-start gap-4">
            {mockData.boards.map((board) => (
              <Card
                id={board.id}
                name={board.name}
                status={board.status}
                count={board.count}
                type={board.type}
              />
            ))}
          </div>
        </div>

        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="2"
          height="260"
          viewBox="0 0 2 260"
          fill="none"
        >
          <path d="M1 0V260" stroke="#98A2B3" />
        </svg>

        <div className="flex flex-col gap-4 items-start w-[440px]">
          <div className="flex flex-col gap-4">
            <div id="sensor" className="">
              <h2>เซนเซอร์</h2>
              <div className="flex items-center">
                <p className="text-2xl font-bold leading-8">
                  ทั้งหมด
                  <span className="text-3xl font-bold ps-1.5 pe-1.5">
                    {data.sensor}
                  </span>
                  เซนเซอร์
                </p>
              </div>
            </div>
          </div>
          <div className="flex items-start gap-4">
            {mockData.sensors.map((sensor) => (
              <Card
                id={sensor.id}
                name={sensor.name}
                status={sensor.status}
                count={sensor.count}
                type={sensor.type}
              />
            ))}
          </div>
        </div>
      </div>
      <div className="flex flex-col gap-6 self-stretch w-full">
        <div className="flex flex-col items-start">
          <div className="flex justify-between items-center self-stretch">
            <span className="text-xl leading-7 font-semibold">
              สถิติจำนวนบอร์ด และเซนเซอร์ของ Axons IoT ทั้งหมด
            </span>
            <Dropdown />
          </div>
          <div id="tab-menu-chart" className="w-full">
            <Tab type="chart"></Tab>
          </div>
        </div>
        <div id="chart-dashboard" className="flex self-stretch h-[256px]">
          <Chart />
        </div>
        <div
          id="chart-details"
          className="flex justify-center items-start gap-8 self-stretch"
        >
          <div className="flex h-[18px] bg-red-300 w-full"></div>
          <div className="flex h-[18px] bg-green-300 w-full"></div>
        </div>
      </div>
    </div>
  );
}
