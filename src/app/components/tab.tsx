import React from "react";

type Props = {
  type: string;
};

const mockDataMap = ["ทั้งหมด", "ประเทศไทย"];

export default function Tap({ type }: Props) {
  return (
    <div className="flex flex-row">
      {type === "map" ? (
        <div className="flex border-b under-line w-full">
          <a href="#" id="all-country" className="flex p-4">
            ทั้งหมด
          </a>
          <a href="#" id="thai-country" className="flex p-4">
            ประเทศไทย
          </a>
        </div>
      ) : type === "chart" ? (
        <div className="flex border-b under-line w-full">
          <a href="#" id="all-country" className="flex p-4">
            รายเดือน
          </a>
          <a href="#" id="thai-country" className="flex p-4">
            รายปี
          </a>
        </div>
      ) : (
        ""
      )}
    </div>
  );
}
